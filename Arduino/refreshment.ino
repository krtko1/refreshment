#include <EEPROM.h>
#include <SPI.h>
#include <LiquidCrystal.h>
#include <MFRC522.h>

#define MAIN             0
#define BUY              1
#define CHARGE           2
#define WITHDRAW         3
#define SELL             4
#define STATUS           5
#define ITEM_INFO        6
#define DETERMINE_AMOUNT 7
#define ACCEPT           8

#define NAN -2147483648

MFRC522 mfrc522(10, 8);
MFRC522::MIFARE_Key keyA = {255,255,255,255,255,255};
MFRC522::MIFARE_Key keyB = {255,255,255,255,255,255};//only in development


LiquidCrystal lcd(7, 6, 5, 4, 3, 2);

const int inputPin = A0;
const int ledPin =  9;

int8_t state = MAIN;
int8_t button = 0;
int32_t line = 0;
int8_t id = 0;
int32_t credit = NAN;
int32_t difference = 0;
bool buy = 0;

String main_menu[] = {
    "Co dalej?       ",
    "Kupit tovar?    ",
    "Nabit kredit?   ",
    "Vybrat kredit?  ",
    "Doplnit tovar?  ",
    "Aktualny stav?  ",
};

struct item {
    char name[17];
    int32_t price;
    uint8_t amount;
};

item items[] = {
    {"Horalka         ",  40, 0},
    {"Zelatina        ", 299, 0},
    {"polievka kuracia",  25, 0},
    {"polievka syrova ",  25, 0},
};

void setup() {
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin, HIGH);
    lcd.begin(16, 2);
    Serial.begin(115200);
    SPI.begin();
    mfrc522.PCD_Init();
    delay(4);
    mfrc522.PCD_DumpVersionToSerial();

    Serial.println(F("Používané klúče (A, B):"));
    Serial.print(F("A:"));
    dump_byte_array(keyA.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println();
    Serial.print(F("B:"));
    dump_byte_array(keyB.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println();
    for (int16_t i = 0; i < sizeof(items) / sizeof(item); i++) {
        items[i].amount = EEPROM.read(i);
    }
}

void loop() {
    button = check_button_state();
    switch (button) {
        case 1:
            break;
        case 2:
            line++;
            break;
        case 3:
            line--;
            break;
        case 4:
            state = MAIN;
            line = 0;
            credit = NAN;
            difference = 0;
            id = 0;
            break;
        case 5:
            break;
        default:
            break;
    }

    switch (state) {
        case MAIN:
            switch (button) {
                case 1:
                    state+=line;
                    line = 0;
                    lcd.setCursor(0, 1);
                    lcd.print("                ");
                    break;
                case 2:
                    line %= sizeof(main_menu) / sizeof(main_menu[0]);
                    break;
                case 3:
                    if (line < 0) line += sizeof(main_menu) / sizeof(main_menu[0]);
                    break;
                default:
                    lcd.setCursor(0, 0);
                    lcd.print("Vitaj v obchode!");
                    lcd.setCursor(0, 1);
                    lcd.print(main_menu[line]);
                    break;
            }
            break;
        case BUY:
            switch (button) {
                case 1:
                    state=ITEM_INFO;
                    buy = 1;
                    id = line;
                    line = 0;
                    break;
                case 2:
                    line %= sizeof(items) / sizeof(item);
                    break;
                case 3:
                    if (line < 0) line += sizeof(items) / sizeof(item);
                    break;
                case 4:
                    break;
                default:
                    lcd.setCursor(0, 0);
                    lcd.print("Co kupit?       ");
                    lcd.setCursor(0, 1);
                    lcd.print(items[line].name);
                    break;
            }
            break;
        case CHARGE:
            switch (button) {
                case 1:
                    state = STATUS;
                    break;
                case 2:
                    if(difference > 0) difference-=5;
                    break;
                case 3:
                    difference+=5;
                    break;
                case 4:
                    break;
                default:
                    lcd.setCursor(0, 0);
                    lcd.print("Kolko nabit?    ");
                    break;
            }
            print_credit(difference);
            break;
        case WITHDRAW:
            switch (button) {
                case 1:
                    state = STATUS;
                    break;
                case 2:
                    if(difference < 0) difference+=5;
                    break;
                case 3:
                    difference-=5;
                    break;
                case 4:
                    break;
                default:
                    lcd.setCursor(0, 0);
                    lcd.print("Kolko vybrat?   ");
                    break;
            }
            print_credit(-difference);
            break;
        case SELL:
            switch (button) {
                case 1:
                    state=ITEM_INFO;
                    buy = 0;
                    id = line;
                    line = 0;
                    break;
                case 2:
                    line %= sizeof(items) / sizeof(item);
                    break;
                case 3:
                    if (line < 0) line += sizeof(items) / sizeof(item);
                    break;
                case 4:
                    break;
                default:
                    lcd.setCursor(0, 0);
                    lcd.print("Co doplnit?     ");
                    lcd.setCursor(0, 1);
                    lcd.print(items[line].name);
                    break;
            }
            break;
        case STATUS:
            switch (button) {
                case 4:
                    break;
                default:
                    if (credit != NAN) {
                        lcd.setCursor(0, 0);
                        lcd.print("Aktualny kredit:");
                        print_credit(credit);
                    } else {
                        lcd.setCursor(0, 0);
                        lcd.print("Priloz kartu!   ");
                        lcd.setCursor(0, 1);
                        lcd.print("                ");
                        credit = load_credit(difference);
                    }
                    break;
            }
            break;
        case ITEM_INFO:
            switch (button) {
                case 1:
                    state = DETERMINE_AMOUNT;
                    line = 0;
                    break;
                case 2:
                    line%=3;
                    break;
                case 3:
                    if (line < 0) line = 2;
                    break;
                case 4:
                    break;
                default:
                    lcd.setCursor(0, 0);
                    lcd.print(items[id].name);
                    if (items[id].amount == 0 && buy) {
                        lcd.setCursor(0, 1);
                        lcd.print("tovar nedostupny");
                    } else {
                        switch (line) {
                            case 0:
                                print_credit(items[id].price);
                                break;
                            case 1:
                                lcd.setCursor(0, 1);
                                lcd.print("urcit mnozstvo  ");
                                break;
                            case 2:
                                lcd.setCursor(0, 1);
                                lcd.print("skladom: ");
                                char buf[8];
                                sprintf(buf, "%7.1d", items[id].amount);
                                lcd.print(buf);
                                break;
                            default:
                                break;
                        }
                    }
                    break;
            }
            break;
            case DETERMINE_AMOUNT:
            switch (button) {
                case 1:
                    state = ACCEPT;
                    if (buy) line *= -1;
                    difference = -line*items[id].price;
                    items[id].amount -= line;
                    EEPROM.update(id, items[id].amount);
                    line = 0;
                    break;
                case 2:
                    if (line > 0) line = 0;
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    lcd.setCursor(0, 0);
                    lcd.print(items[id].name);
                    char buf[17];
                    lcd.setCursor(0, 1);
                    sprintf(buf, "%10.1d kusov", -line);
                    lcd.print(buf);
                    break;
            }
            break;
            case ACCEPT:
            switch (button) {
                case 1:
                    state = STATUS;
                    line = 0;
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    lcd.setCursor(0, 0);
                    if (buy) {
                        lcd.print("zaplatis:       ");
                        print_credit(-difference);
                    } else {
                        lcd.print("doplni sa ti:   ");
                        print_credit(difference);
                    }
                    break;
            }
            break;
        default:
            break;
    }
    
    if (button) {
        if (difference) delay(100);
        else delay(200);
    }
    
}

uint8_t check_button_state() {
    const uint16_t val = analogRead(inputPin);
    if(val < 100 && val >=  0) return 1;
    if(val < 250 && val > 150) return 2;
    if(val < 470 && val > 370) return 3;
    if(val < 670 && val > 570) return 4;
    if(val < 870 && val > 770) return 5;
    return 0;
}

int32_t load_credit(int32_t dif) {
    byte valueBlock;
    byte nameBlock;
    byte trailerBlock;
    byte buffer[18];
    byte size = sizeof(buffer);
    int32_t value;
    
    if (!mfrc522.PICC_IsNewCardPresent()) return NAN;
    if (!mfrc522.PICC_ReadCardSerial()) return NAN;
    lcd.setCursor(0, 1);
    lcd.print("Nacitava sa...  ");
    
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI && piccType != MFRC522::PICC_TYPE_MIFARE_1K&& piccType != MFRC522::PICC_TYPE_MIFARE_4K) return card_error();

    for (uint8_t i = 1; i < 16; i++) {
        valueBlock   = 4*i + 1;
        nameBlock    = 4*i;
        trailerBlock = 4*i + 3;

        if (mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &keyA, &(mfrc522.uid)) != MFRC522::STATUS_OK) continue;

        byte trailerBuffer[] = {
            keyA.keyByte[0], keyA.keyByte[1], keyA.keyByte[2], keyA.keyByte[3], keyA.keyByte[4], keyA.keyByte[5],
            24, 119, 142,                       // Access Bits
            0,                                  // User data
            keyB.keyByte[0], keyB.keyByte[1], keyB.keyByte[2], keyB.keyByte[3], keyB.keyByte[4], keyB.keyByte[5]};

        if(mfrc522.MIFARE_Read(trailerBlock, buffer, &size) != MFRC522::STATUS_OK) return card_error();

        if (buffer[6] != trailerBuffer[6] || buffer[7] != trailerBuffer[7] || buffer[8] != trailerBuffer[8])
            if (mfrc522.MIFARE_Write(trailerBlock, trailerBuffer, 16) != MFRC522::STATUS_OK) return card_error();

        if (mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, trailerBlock, &keyB, &(mfrc522.uid)) != MFRC522::STATUS_OK) continue;

        byte nameBuffer[] = "Refreshment app ";

        if(mfrc522.MIFARE_Read(nameBlock, buffer, &size) != MFRC522::STATUS_OK) return card_error();
        
        for (uint8_t i = 0; i < 16; i++) {
            if(buffer[i] != nameBuffer[i]) {
                if (mfrc522.MIFARE_Write(nameBlock, nameBuffer, 16) != MFRC522::STATUS_OK) return card_error();
                break;
            }
        }
    }

    formatValueBlock(valueBlock);

    if (mfrc522.MIFARE_GetValue(valueBlock, &value) != MFRC522::STATUS_OK) return card_error();

    if (dif > 0) {
        if (mfrc522.MIFARE_Increment(valueBlock, dif) != MFRC522::STATUS_OK) return card_error();
        if (mfrc522.MIFARE_Transfer(valueBlock) != MFRC522::STATUS_OK) return card_error();
    } else if (dif < 0) {
        if (mfrc522.MIFARE_Decrement(valueBlock, -dif) != MFRC522::STATUS_OK) return card_error();
        if (mfrc522.MIFARE_Transfer(valueBlock) != MFRC522::STATUS_OK) return card_error();
    } 

    mfrc522.PICC_HaltA();
    mfrc522.PCD_StopCrypto1();
    log_transaction(mfrc522.uid, value, dif);
    return value+dif;
}

void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}

void formatValueBlock(byte blockAddr) {
    byte buf[18];
    byte size = sizeof(buf);

    if (mfrc522.MIFARE_Read(blockAddr, buf, &size) != MFRC522::STATUS_OK) return card_error();

    if ((buf[0] != (byte)~buf[4]) || (buf[1] != (byte)~buf[5]) || (buf[2] != (byte)~buf[6]) || (buf[3] != (byte)~buf[7]) ||
        (buf[0] != buf[8]) || (buf[1] != buf[9]) || (buf[2] != buf[10]) || (buf[3] != buf[11]) ||
        (buf[12] != (byte)~buf[13]) || (buf[12] != buf[14]) || (buf[12] != (byte)~buf[15])) {
        byte valueBlock[] = {
            0, 0, 0, 0,
            255, 255, 255, 255,
            0, 0, 0, 0,
            blockAddr, ~blockAddr, blockAddr, ~blockAddr
        };
        if (mfrc522.MIFARE_Write(blockAddr, valueBlock, 16) != MFRC522::STATUS_OK) return card_error();
    }
}

void log_transaction(MFRC522::Uid uid, int32_t initial, int32_t dif) {
    Serial.print("Card UID:");
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.print(" Initial credit: ");
    Serial.print(initial);
    Serial.print(" New credit: ");
    Serial.print(initial + dif);
    Serial.print(" Ballance: ");
    Serial.print(dif);
    Serial.println();
}

int32_t card_error(){
    mfrc522.PICC_HaltA();
    delay(100);
    mfrc522.PCD_StopCrypto1();
    delay(100);
    mfrc522.PCD_Reset();
    delay(100);
    mfrc522.PCD_Init();
    lcd.setCursor(0, 0);
    lcd.print("Nepodporovana   ");
    lcd.setCursor(0, 1);
    lcd.print("karta!          ");
    delay(1000);
    return NAN;
}

void print_credit(int32_t val) {
    char buf[9];
    lcd.setCursor(0, 1);
    sprintf(buf, "%8.1d", (val < 0)? -val/100 : val/100);
    if (val < 0) buf[0] = '-';
    else buf[0] = '+';
    lcd.print(buf);
    sprintf(buf, ".%.2d eur ", (val < 0)? -val%100 : val%100);
    lcd.print(buf);
}
